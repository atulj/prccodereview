﻿using PRCPayment.Models;
using PRCPayment.ViewModels;
using System;
using System.Web.Mvc;

namespace PRCPayment.Controllers
{
    public class ContactUSController : Controller
    {
        // GET: ContactUS
        [Route("ContactUS")]
        public ActionResult Index()
        {
            
            ContactUSViewModel model = new ContactUSViewModel();
            if(TempData["Iscontacus"]!=null)
            {
                ViewBag.Iscontacus = TempData["Iscontacus"];
                TempData.Remove("Iscontacus");
            }
           
            return View(model);
        }
        //ContactUS Post Action
        [HttpPost]
        [Route("ContactUS")]
        public ActionResult Index(ContactUSViewModel model)
        {
            //model.registrationModel.EmailURL = Request.Url.Scheme + "://" + Request.Url.Authority + "/" + "Account/IsVerifyEmail/";
            model.objModel = model.ContactUS(model.objModel); 
            TempData["Iscontacus"]= model.objModel.ErrorCode;
            return RedirectToAction("Index",model);           
        }
    }
}