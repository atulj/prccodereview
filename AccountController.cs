﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PRCPayment.Models;
using PRCPayment.ViewModels;

namespace PRCPayment.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        [Route("Account")]
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult PRC()
        {
            return RedirectToAction("Registration");
        }

        [Route("Payment")]
        public ActionResult Registration()
        {           
            return View();
        }
        public ActionResult PayBill(CustomerViewModel model)
        {
            Session.Clear();
            var isAjax = Request.IsAjaxRequest();
            if (isAjax)
            {
               // model.payBillRegistrationModel = model.PayBill(model.payBillRegistrationModel);
                PayBillRegistrationModel payBillModel = new PayBillRegistrationModel();
                payBillModel = model.payBillRegistrationModel;
                if (model.payBillRegistrationModel.PaymentCode <= 0)
                {
                    payBillModel.ErrorCode = "Error";
                    payBillModel.ErrorMessage = "Invalid payment code, please try again!";
                }
                else
                {
                    payBillModel.ErrorCode = "Success";
                    payBillModel.ErrorMessage = "Payment code successfully matched.";
                }
                if (model.payBillRegistrationModel.PaymentCode > 0)
                {
                    Session["PaymentCodeSession"] = model.payBillRegistrationModel.PaymentCode;
                }                
                if (model.payBillRegistrationModel.ErrorCode == "Success")
                {
                    return PartialView("_PayBill", model);
                }
            }
            return PartialView("_PayBill", model);
        }
    }
}