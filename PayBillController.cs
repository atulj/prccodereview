﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PRCPayment.Helper;
using PRCPayment.ViewModels;
using System.Data.Entity;
using AuthorizeNet.Api.Contracts.V1;
using AuthorizeNet.Api.Controllers;
using AuthorizeNet.Api.Controllers.Bases;
//using PRCPayment.DAL;

namespace PRCPayment.Controllers
{
    public class PayBillController : Controller
    {
        // GET: PayBill
        [Route("Payment/{PaymentCode}")]
        public ActionResult PayBill(long? PaymentCode)
        {
            if ((PaymentCode ==null) )
            {
                return RedirectToAction("Registration", "Account");
            }
            if (PaymentCode == null)
            {
                return RedirectToAction("Notfound");
            }
            if ((PaymentCode != null))
            {
                Session["PaymentCodeSession"] = PaymentCode;                
            }
            PayBillViewModel model = new PayBillViewModel();           
            model = model.GetPayBillDetail(PaymentCode.Value,0);
            if (model.ErrorCode == "Error")
            {
                return RedirectToAction("Registration", "Account");
            }
            else if (model.payBillModel.PaymentCode == 0)
            {
                return RedirectToAction("Notfound");
            }
            return View(model);
        }        

        [Route ("Notfound")]
        //Not found action for show Not found Page
        public ActionResult Notfound()
        {
            return View();
        }

        public ActionResult Savepayment(String ApiLoginID, String ApiTransactionKey, string amount, string dataDesc,string dataValue, string PaymentType,
                                        string paymentcode,string emailaddress, int saveCardChecked, string bankName,
                                        string accountNumber, string nameOnAccount, string billingZipCode,
                                        string expMonth, string expYear, string accountType, string bankRoutingNumber,
                                        string type,string cardtype)
        {
            Console.WriteLine("Running VisaCheckoutTransaction Sample ...");
            string rtmessage = "";
            PaymentReceiveViewmodel umvm = new PaymentReceiveViewmodel();
            // The test setup.
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = ApiLoginID,
                ItemElementName = ItemChoiceType.transactionKey,
                Item = ApiTransactionKey,
            };

            ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.SANDBOX;


            //set up data based on transaction
            var transactionAmount = Convert.ToDecimal(amount);
            var opaqueDataType = new opaqueDataType
            {
                dataDescriptor = dataDesc, // "COMMON.ACCEPT.INAPP.PAYMENT",
               // dataKey = "NQzcMISSxLX789w+CGX+tXi3lKntO1dpZbZaREOUprVRByJkg1xnpc2Wx9aT5/BLOxQmHqmIsjjy+tF6HqKKGwovvXjIS3fE3y3tBRNbz8D7y6vYMup+AWbEvZqDEBSi",
               dataValue=dataValue
            };

            //standard api call to retrieve response
            var paymentType = new paymentType { Item = opaqueDataType };
            var transactionRequest = new transactionRequestType
            {
                transactionType = transactionTypeEnum.authCaptureTransaction.ToString(),
                payment = paymentType,
                amount = transactionAmount,
            };
            var request = new createTransactionRequest { transactionRequest = transactionRequest };
            var controller = new createTransactionController(request);
            controller.Execute();
            var response = controller.GetApiResponse();

            // validate response
            if (response != null)
            {                
                if (response.messages.resultCode == messageTypeEnum.Ok)
                {
                    if (response.transactionResponse.messages != null)
                    {                        
                        long pcode = Convert.ToInt64(paymentcode);
                        umvm = umvm.PaymentReceiveSave(DateTime.Now.Date, response.transactionResponse.transId, pcode, PaymentType, transactionAmount,
                                                        emailaddress,saveCardChecked, bankName, accountNumber, nameOnAccount, billingZipCode,
                                                        expMonth, expYear, accountType,bankRoutingNumber, type, cardtype, "0", umvm);
                        
                       
                       return Json(new
                        {
                            status = true,
                            TransactionResponseCode = response.transactionResponse.messages[0].code,
                            MessageCode = response.transactionResponse.messages[0].code,
                            AuthCode = response.transactionResponse.authCode,
                            Description = response.transactionResponse.messages[0].description,
                            transaction_id = response.transactionResponse.transId
                        }, JsonRequestBehavior.AllowGet);

                    }
                    else
                    {
                        rtmessage = "1:"+ response.transactionResponse.errors[0].errorText;
                        Console.WriteLine("Failed Transaction.");
                        if (response.transactionResponse.errors != null)
                        {
                            Console.WriteLine("Error Code: " + response.transactionResponse.errors[0].errorCode);
                            Console.WriteLine("Error message: " + response.transactionResponse.errors[0].errorText);
                            return Json(new { success = false, responseText = response.transactionResponse.errors[0].errorText }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                else
                {
                    rtmessage = "2:" + response.transactionResponse.errors[0].errorText;
                    Console.WriteLine("Failed Transaction.");
                    if (response.transactionResponse != null && response.transactionResponse.errors != null)
                    {
                        Console.WriteLine("Error Code: " + response.transactionResponse.errors[0].errorCode);
                        Console.WriteLine("Error message: " + response.transactionResponse.errors[0].errorText);
                        return Json(new { success = false, responseText = response.transactionResponse.errors[0].errorText }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        rtmessage = "3:" + response.messages.message[0].text;
                        Console.WriteLine("Error Code: " + response.messages.message[0].code);
                        Console.WriteLine("Error message: " + response.messages.message[0].text);
                        return Json(new { success = false, responseText = response.transactionResponse.errors[0].errorText }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else
            {                
                Console.WriteLine("Null Response.");
                return Json(new { success = false, responseText = "Null Response." }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { success = false, responseText = "Null Response." }, JsonRequestBehavior.AllowGet);
        }
                
        //Save Paybill value in DB
        public ActionResult SavePayBill(string payamount, string transactionid, string paymenttype, string paymentcode,
                                        string emailaddress, int saveCardChecked, string bankName,
                                        string accountNumber, string nameOnAccount, string billingZipCode,
                                        string expMonth, string expYear, string accountType, string bankRoutingNumber, 
                                        string type, string cardtype, string response,PaymentReceiveViewmodel umvm)
        {
            string rqPracticeId = "";
            decimal pamount = 0;
            try
            {
                if (type.ToUpper() == "MOBILEPAY")
                {
                    rqPracticeId = Session["MPPracticeId"].ToString();
                }
                else if (type.ToUpper() == "PAYBILL")
                {
                    rqPracticeId = "0";// SessionHelper.GetloggedPracticeID().ToString();
                }
                long pcode = Convert.ToInt64(paymentcode);
                if (payamount != "" || Convert.ToDecimal(payamount) > 0)
                {
                    pamount = Convert.ToDecimal(payamount);
                }

                umvm = umvm.PaymentReceiveSave(DateTime.Now.Date, transactionid, pcode, paymenttype, pamount, emailaddress,
                                                saveCardChecked, bankName, accountNumber, nameOnAccount, billingZipCode,
                                                 expMonth, expYear, accountType, bankRoutingNumber, type, cardtype, rqPracticeId, umvm);
                if (umvm.FinalErrorFlag == "error")
                {
                    ViewBag.Message = umvm.FinalErrorMessage;
                    return Json(new { success = false, responseText = umvm.FinalErrorMessage }, JsonRequestBehavior.AllowGet);
                    // return RedirectToAction("Registration", "Account");
                }
                else if (umvm.FinalErrorFlag == "success")
                {
                    TempData["PaymentCode"] = paymentcode;
                    return Json(new { success = true, responseText = umvm.FinalErrorMessage }, JsonRequestBehavior.AllowGet);
                    //return RedirectToAction("Index", "Dashboard");
                }
                return null;

            }
            catch (Exception ex)
            {
                // return null;
                return Json(new { success = false, responseText = umvm.FinalErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        } 
    }  
}